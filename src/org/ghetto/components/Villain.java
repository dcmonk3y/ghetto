package org.ghetto.components;

import java.util.Random;

import org.ghetto.services.CharacterService;
import org.ghetto.services.GameEngineService;
import org.ghetto.services.RequireCharacterService;
import org.ghetto.services.RequireGameEngineService;
import org.ghetto.services.VillainService;
import org.ghetto.tools.Vec3;

public class Villain extends Character implements VillainService,
        RequireCharacterService, RequireGameEngineService {

    private GameEngineService m_gameEngine;
    private CharacterService m_hero;
    private float m_mvt;
    private int m_waitTime;

    public Villain(String name, Vec3 pos, float dim, float speed, int life,
            int power) {

        super(name, pos, dim, speed, life, power);
    }

    // constructors

    @Override
    public void init(String name, Vec3 pos, float dim, float speed, int life,
            int power) {

        super.init(name, pos, dim, speed, life, power);
        this.m_gameEngine = null;
        this.m_hero = null;
        this.m_mvt = 0.f;
        this.m_waitTime = 0;
    }

    // operators

    @Override
    public void think(int ms) {

        Random rand = new Random();

        if (this.isDead()) {
            return;
        }

        this.updateState(ms);

        if (Vec3.distance(m_hero.getPos(), this.m_pos) < 20.f) {
            this.m_mvt = 0.f;
            this.m_waitTime -= ms;
            if (this.m_waitTime <= 0) {
                this.m_gameEngine.characterBeat(this);
                this.m_waitTime = 1000;
            }
        } else if (Vec3.distance(m_hero.getPos(), this.m_pos) < 100.f) {
            this.m_dir = new Vec3(Vec3.sub(this.m_hero.getPos(), this.m_pos));
            this.m_mvt = Vec3.norm(this.m_dir);
            this.m_waitTime = 500;
        } else {
            if (rand.nextInt(50) == 0) {
                if (this.m_mvt > 0.f == false) {
                    this.m_dir = new Vec3(rand.nextFloat() - 0.5f,
                            rand.nextFloat() - 0.5f, 0.f);
                    this.m_mvt = (20 + rand.nextInt(20)) * rand.nextFloat();
                }
            }
        }
    }

    // methods

    private void updateState(int ms) {

        if (this.m_mvt > 0.f) {
            Vec3 dir = Vec3.normalize(this.getDir());
            dir = Vec3.scale(dir, this.m_speed * ms / 1000.f);
            //delegate passed in, not contract
            this.m_gameEngine.characterMove(this, dir);
            this.m_mvt -= Vec3.norm(dir);
            this.m_mvt = this.m_mvt < 0.f ? 0.f : this.m_mvt;
        }
    }

    // required services

    @Override
    public void bindGameEngineService(GameEngineService gameEngine) {

        this.m_gameEngine = gameEngine;
    }

    @Override
    public void bindCharacterService(CharacterService character) {

        this.m_hero = character;
    }
}
