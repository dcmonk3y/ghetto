package org.ghetto.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.ghetto.services.BlockService;
import org.ghetto.services.CharacterService;
import org.ghetto.services.DrawableService;
import org.ghetto.services.EventsService;
import org.ghetto.services.GameEngineService;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.services.RequireCharacterService;
import org.ghetto.services.RequireDrawableService;
import org.ghetto.services.RequireEventsService;
import org.ghetto.services.RequireMapService;
import org.ghetto.services.RequireVillainService;
import org.ghetto.services.VillainService;
import org.ghetto.tools.Vec3;

public class GameEngine implements GameEngineService, RequireEventsService,
        RequireCharacterService, RequireVillainService, RequireMapService,
        RequireDrawableService {

    private EventsService m_events;
    private CharacterService m_character;
    private VillainService m_slick;
    private ArrayList<VillainService> m_villains;
    private ArrayList<CharacterService> m_allCharacters;
    private ArrayList<DrawableService> m_drawables;
    private MapService m_map;

    public GameEngine() {

        this.init();
    }

    // observators
    @Override
    public boolean isEnd() {

        if (this.m_character == null || this.m_slick == null)
            return false;

        return this.m_character.isDead() || this.m_slick.isDead();
    }

    @Override
    public boolean youHaveLost() {

        if (this.isEnd() == false) {
            return false;
        }

        if (this.m_character.isDead() == false && this.m_slick.isDead()) {
            return false;
        }

        return this.m_character.isDead();
    }

    // constructors
    @Override
    public void init() {

        this.m_map = null;
        this.m_events = null;
        this.m_character = null;
        this.m_slick = null;
        this.m_villains = new ArrayList<VillainService>();
        this.m_allCharacters = new ArrayList<CharacterService>();
        this.m_drawables = new ArrayList<DrawableService>();
    }

    // operators
    @Override
    public void update(int ms) {

        for (CharacterService guy : this.m_allCharacters) {
            this.characterUpdate(guy, ms);
        }

        Vec3 dir = new Vec3();

        if (this.m_events.getEventState("UP")) {
            dir = Vec3.add(dir, new Vec3(0.f, 1.f, 0.f));
        }
        if (this.m_events.getEventState("DOWN")) {
            dir = Vec3.add(dir, new Vec3(0.f, -1.f, 0.f));
        }
        if (this.m_events.getEventState("LEFT")) {
            dir = Vec3.add(dir, new Vec3(-1.f, 0.f, 0.f));
        }
        if (this.m_events.getEventState("RIGHT")) {
            dir = Vec3.add(dir, new Vec3(1.f, 0.f, 0.f));
        }

        try {
            if (Vec3.norm(dir) > Vec3.EPSILON) {
                dir = Vec3.normalize(dir);
                dir = Vec3
                        .scale(dir, this.m_character.getSpeed() * ms / 1000.f);
                this.characterMove(this.m_character, dir);
            }
        } catch (Error e) {
            System.err.println(e);
        }

        ArrayList<ObjectService> objects;
        objects = this.m_map.getObjects(this.m_character.getPos(),
                this.m_character.getDim() * 2.f);

        for (ObjectService object : objects) {
            try {
                if (object.isEquipable() == false) {
                    if (object.getType() == ObjectService.OBJECT_TYPE.LIFE) {
                        this.m_character.addLife(1);
                    }
                    if (object.getType() == ObjectService.OBJECT_TYPE.MONEY) {
                        this.m_character.addMoney(1);
                    }
                    this.m_map.removeObject(object);
                }
            } catch (Error e) {
                System.err.println(e);
            }
        }

        try {
            if (this.m_events.getEventState("GRAB")) {
                for (ObjectService object : objects) {
                    if (object.isEquipable()) {
                        if (this.m_character.grab(object)) {
                            this.m_map.removeObject(object);
                            break;
                        }
                    }
                }
            }
        } catch (Error e) {
            System.err.println(e);
        }

        try {
            if (this.m_events.getEventState("DROP")) {
                ObjectService object = this.m_character.getHands();
                if (object != null) {
                    Vec3 pos = Vec3.normalize(this.m_character.getDir());
                    pos = Vec3.scale(pos, this.m_character.getDim() * 3.f);
                    pos = Vec3.add(this.m_character.getPos(), pos);
                    this.m_map.addObject(object, pos);
                    this.m_character.drop();
                }
            }
        } catch (Error e) {
            System.err.println(e);
        }

        if (this.m_events.getEventState("BEAT")) {
            try {
                this.characterBeat(this.m_character);
            } catch (Error e) {
                System.err.println(e);
            }
        }

        for (VillainService villain : this.m_villains) {
            try {
                villain.think(ms);
            } catch (Error e) {
                System.err.println(e);
            }
        }
    }

    @Override
    public void render() {

        Collections.sort(this.m_drawables, new Comparator<DrawableService>() {
            @Override
            public int compare(DrawableService d1, DrawableService d2) {
                if (d1.getPos().y() < d2.getPos().y()) {
                    return 1;
                }
                return -1;
            }
        });

        for (DrawableService drawable : this.m_drawables) {

            drawable.render();
        }
    }

    @Override
    public void characterMove(CharacterService character, Vec3 dir) {

        checkedMove(character, dir, new MoveFun() {
            @Override
            public void move(CharacterService character, Vec3 dir) {
                character.move(dir);
            }
        });
    }

    @Override
    public void characterBeat(CharacterService character) {

        if (character.beat() == false) {
            return;
        }

        for (CharacterService guy : this.m_allCharacters) {

            // if same guy, or dead, or the pos is exactly the same such that it
            // is the same guy.
            // needed due to the fact we use
            // this.m_gameEngine.characterMove(this, dir); in
            // villain class. 'this' will be the delegate which will not equal
            // the character contract
            // object
            if (guy == character || guy.isDead()
                    || guy.getPos() == character.getPos()) {
                continue;
            }

            Vec3 guyPos = guy.getPos();
            Vec3 charPos = character.getPos();
            Vec3 push = Vec3.sub(guyPos, charPos);

            if (collision(character, guy)) {
                push = Vec3.normalize(push);
                push = Vec3.scale(push, (guy.getSpeed() / 1000.f));
                guy.addLife(-character.getPower());
                guy.addFreezeTime(250);
                this.characterDrift(guy, push);
            }
            
        }
    }
    
    @Override
    public boolean collision(CharacterService character, CharacterService guy){
        Vec3 guyPos = guy.getPos();
        Vec3 charPos = character.getPos();
        Vec3 push = Vec3.sub(guyPos, charPos);

        if (push.x() * character.getDir().x() >= 0) {

            float guyDim = guy.getDim();
            float charDim = character.getDim() * 2.f;
            float distX = Math.abs(push.x());
            float distY = Math.abs(push.y());

            if (distX < charDim + guyDim && distY < charDim + guyDim) {
                return true;
            }
        }
        return false;
    }

    // methods
    private void characterUpdate(CharacterService character, int ms) {

        character.addFreezeTime(-ms);
        if (character.getFreezeTime() == 0) {
            character.stop();
        } else {
            if (character.isMoving()) {
                Vec3 dir = Vec3.normalize(character.getDriftDir());
                dir = Vec3.scale(dir, character.getSpeed() * ms / 1000.f);
                this.characterDrift(character, dir);
            }
        }
    }

    private interface MoveFun {

        public void move(CharacterService character, Vec3 dir);
    }

    private void characterDrift(CharacterService character, Vec3 dir) {

        checkedMove(character, dir, new MoveFun() {
            @Override
            public void move(CharacterService character, Vec3 dir) {
                character.drift(dir);
            }
        });
    }

    private void checkedMove(CharacterService character, Vec3 dir, MoveFun fun) {

        Vec3 pos = Vec3.add(character.getPos(), dir);
        BlockService block = this.m_map.getBlockAtPos(pos);

        if (block == null) {
            return;
        }

        if (block.getType() == BlockService.BLOCK_TYPE.EMPTY) {

            boolean flag = true;

            for (CharacterService guy : this.m_allCharacters) {

                // if same guy, or dead, or the pos is exactly the same such
                // that it is the same guy.
                // needed due to the fact we use
                // this.m_gameEngine.characterMove(this, dir); in
                // villain class. 'this' will be the delegate which will not
                // equal the character contract
                // object
                if (guy == character || guy.isDead()
                        || guy.getPos() == character.getPos()) {
                    continue;
                }

                Vec3 guyPos = guy.getPos();
                float guyDim = guy.getDim();
                float charDim = character.getDim();
                float distX = Math.abs(guyPos.x() - pos.x());
                float distY = Math.abs(guyPos.y() - pos.y());

                if (distX < charDim + guyDim && distY < charDim + guyDim) {
                    flag = false;
                    break;
                }
            }

            if (flag) {
                fun.move(character, dir);
            }
        } else {
            fun.move(character, dir);
            character.addLife(-character.getLife());
        }
    }

    // required services
    @Override
    public void bindEventsService(EventsService events) {

        this.m_events = events;
    }

    @Override
    public void bindCharacterService(CharacterService character) {

        this.m_character = character;
        this.m_allCharacters.add(character);
    }

    @Override
    public void bindVillainService(VillainService villain) {

        if (this.m_villains.isEmpty()) {
            this.m_slick = villain;
        }

        this.m_villains.add(villain);
        this.m_allCharacters.add(villain);
    }

    @Override
    public void bindMapService(MapService map) {

        this.m_map = map;
    }

    @Override
    public void bindDrawableService(DrawableService drawable) {

        this.m_drawables.add(drawable);
    }
}
