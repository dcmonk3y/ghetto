package org.ghetto.components;

import java.util.ArrayList;
import java.util.HashMap;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Resource;
import org.ghetto.tools.Vec3;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Object implements ObjectService {

    private static HashMap<String, Animation> ANIMATIONS;

    static {

        Object.ANIMATIONS = new HashMap<String, Animation>();

        try {

            ArrayList<SpriteSheet> sheets = new ArrayList<SpriteSheet>();

            sheets.add(new SpriteSheet(Resource.get(
                    "img/" + "MONEY" + ".png").getPath(), 8, 11, 8));
            sheets.add(new SpriteSheet(Resource.get(
                    "img/" + "CHAIN" + ".png").getPath(), 15, 17, 25));
            sheets.add(new SpriteSheet(Resource.get(
                    "img/" + "BATTON" + ".png").getPath(), 25, 15, 15));

            ArrayList<Animation> list = new ArrayList<Animation>();

            for (SpriteSheet sheet : sheets) {

                Animation original = new Animation(sheet, 100);
                list.add(original);
            }

            Object.ANIMATIONS.put("MONEY", list.get(0));
            Object.ANIMATIONS.put("CHAIN", list.get(1));
            Object.ANIMATIONS.put("BATTON", list.get(2));

        } catch (SlickException e) {
            System.out.println(e);
        }
    }

    private OBJECT_TYPE m_type;
    private boolean m_equipable;
    private Vec3 m_position;
    private int m_power;
    private boolean m_isInMap;
    private Animation animation;

    public Object(OBJECT_TYPE type, Vec3 pos) {
        this.init(type, pos);
    }

    @Override
    public OBJECT_TYPE getType() {
        return this.m_type;
    }

    @Override
    public boolean isEquipable() {
        return this.m_equipable;
    }

    @Override
    public int getPower() {
        return this.m_power;
    }

    @Override
    public boolean isInMap() {
        return this.m_isInMap;
    }

    @Override
    public Vec3 getPos() {
        return this.m_position;
    }

    @Override
    public void init(OBJECT_TYPE type, Vec3 pos) {
        this.m_type = type;
        this.m_position = pos;
        this.m_isInMap = true;
        switch (type) {
            case MONEY:
                this.m_equipable = false;
                break;
            case CHAIN:
                this.m_equipable = true;
                this.m_power = 2;
                break;
            case BATTON:
                this.m_equipable = true;
                this.m_power = 1;
                break;
            default:
                this.m_equipable = true;
                this.m_power = 1;
                break;
        }
        this.animation = ANIMATIONS.get(m_type.toString());
    }

    @Override
    public void setPos(Vec3 pos) {
        this.m_position = pos;
    }

    @Override
    public void setInMap(boolean in) {
        this.m_isInMap = in;
    }

    @Override
    public void render() {
        if (this.m_isInMap) {
            float x = this.getPos().x() - animation.getWidth() / 2.f;
            float y = -this.getPos().y() - animation.getHeight();
            animation.draw(x, y);
        }
    }
}
