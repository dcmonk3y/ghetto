package org.ghetto.decorators;

import java.util.ArrayList;
import org.ghetto.services.BlockService;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Vec3;
import org.newdawn.slick.Graphics;

public abstract class MapDecorator implements MapService {

    private final MapService delegate;

    public MapDecorator(MapService delegate) {
        this.delegate = delegate;
    }

    @Override
    public int getLength() {
        return this.delegate.getLength();
    }

    @Override
    public int getWidth() {
        return this.delegate.getWidth();
    }

    @Override
    public int getBlockSize() {
        return this.delegate.getBlockSize();
    }

    @Override
    public BlockService getBlock(int l, int w) {
        return this.delegate.getBlock(l, w);
    }

    @Override
    public BlockService getBlockAtPos(Vec3 pos) {
        return this.delegate.getBlockAtPos(pos);
    }

    @Override
    public ArrayList<ObjectService> getObjects(Vec3 pos, float rad) {
        return this.delegate.getObjects(pos, rad);
    }

    @Override
    public void init(int length, int width, int blockSize) {
        this.delegate.init(length, width, blockSize);
    }

    @Override
    public void removeObject(ObjectService object) {
        this.delegate.removeObject(object);
    }

    @Override
    public void addObject(ObjectService object, Vec3 pos) {
        this.delegate.addObject(object, pos);
    }

    @Override
    public void render(Graphics g) {
        this.delegate.render(g);
    }

    @Override
    public ArrayList<ObjectService> getObjects() {
        return this.delegate.getObjects();
    }

    @Override
    public ArrayList<BlockService> getBlocks() {
        return this.delegate.getBlocks();
    }

}
