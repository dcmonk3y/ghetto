package org.ghetto.decorators;

import org.ghetto.services.DrawableService;
import org.ghetto.tools.Vec3;

public abstract class DrawableDecorator implements DrawableService {

    private final DrawableService delegate;

    public DrawableDecorator(DrawableService delegate) {
        this.delegate = delegate;
    }

    @Override
    public Vec3 getPos() {
        return this.delegate.getPos();
    }

    @Override
    public void render() {
        this.delegate.render();
    }

}
