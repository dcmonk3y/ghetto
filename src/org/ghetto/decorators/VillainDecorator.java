package org.ghetto.decorators;

import org.ghetto.services.ObjectService;
import org.ghetto.services.VillainService;
import org.ghetto.tools.Vec3;

public abstract class VillainDecorator implements VillainService {

    private final VillainService delegate;

    public VillainDecorator(VillainService delegate) {
        this.delegate = delegate;
    }

    @Override
    public void think(int ms) {
        this.delegate.think(ms);
    }

    @Override
    public String getName() {
        return this.delegate.getName();
    }

    @Override
    public Vec3 getDir() {
        return this.delegate.getDir();
    }

    @Override
    public Vec3 getDriftDir() {
        return this.delegate.getDriftDir();
    }

    @Override
    public float getDim() {
        return this.delegate.getDim();
    }

    @Override
    public float getSpeed() {
        return this.delegate.getSpeed();
    }

    @Override
    public int getPower() {
        return this.delegate.getPower();
    }

    @Override
    public int getFreezeTime() {
        return this.delegate.getFreezeTime();
    }

    @Override
    public int getLife() {
        return this.delegate.getLife();
    }

    @Override
    public int getMoney() {
        return this.delegate.getMoney();
    }

    @Override
    public boolean isDead() {
        return this.delegate.isDead();
    }

    @Override
    public boolean isMoving() {
        return this.delegate.isMoving();
    }

    @Override
    public boolean isBeating() {
        return this.delegate.isBeating();
    }

    @Override
    public ObjectService getHands() {
        return this.delegate.getHands();
    }

    @Override
    public void init(String name, Vec3 pos, float dim, float speed, int life, int power) {
        this.delegate.init(name, pos, dim, speed, life, power);
    }

    @Override
    public void setPos(Vec3 pos) {
        this.delegate.setPos(pos);
    }

    @Override
    public void addFreezeTime(int ms) {
        this.delegate.addFreezeTime(ms);
    }

    @Override
    public void addLife(int life) {
        this.delegate.addLife(life);
    }

    @Override
    public void addMoney(int cash) {
        this.delegate.addMoney(cash);
    }

    @Override
    public void stop() {
        this.delegate.stop();
    }

    @Override
    public boolean drift(Vec3 dir) {
        return this.delegate.drift(dir);
    }

    @Override
    public boolean move(Vec3 dir) {
        return this.delegate.move(dir);
    }

    @Override
    public boolean beat() {
        return this.delegate.beat();
    }

    @Override
    public boolean grab(ObjectService object) {
        return this.delegate.grab(object);
    }

    @Override
    public boolean drop() {
        return this.delegate.drop();
    }

    @Override
    public Vec3 getPos() {
        return this.delegate.getPos();
    }

    @Override
    public void render() {
        this.delegate.render();
    }
}
