package org.ghetto.decorators;

import org.ghetto.services.ObjectService;
import org.ghetto.tools.Vec3;

public abstract class ObjectDecorator implements ObjectService {

    private final ObjectService delegate;

    public ObjectDecorator(ObjectService delegate) {
        this.delegate = delegate;
    }

    @Override
    public OBJECT_TYPE getType() {
        return this.delegate.getType();
    }

    @Override
    public boolean isEquipable() {
        return this.delegate.isEquipable();
    }

    @Override
    public int getPower() {
        return this.delegate.getPower();
    }

    @Override
    public boolean isInMap() {
        return this.delegate.isInMap();
    }

    @Override
    public void init(OBJECT_TYPE type, Vec3 pos) {
        this.delegate.init(type, pos);
    }

    @Override
    public void setPos(Vec3 pos) {
        this.delegate.setPos(pos);
    }

    @Override
    public void setInMap(boolean in) {
        this.delegate.setInMap(in);
    }

    @Override
    public Vec3 getPos() {
        return this.delegate.getPos();
    }

    @Override
    public void render() {
        this.delegate.render();
    }
}
