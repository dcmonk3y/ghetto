package org.ghetto.decorators;

import org.ghetto.services.BlockService;

public abstract class BlockDecorator implements BlockService {

    private final BlockService delegate;

    public BlockDecorator(BlockService delegate) {
        this.delegate = delegate;
    }

    @Override
    public BLOCK_TYPE getType() {
        return this.delegate.getType();
    }

    @Override
    public void init(BLOCK_TYPE type) {
        this.delegate.init(type);
    }

}
