package org.ghetto.decorators;

import org.ghetto.services.EventsService;

public abstract class EventsDecorator implements EventsService {

    private final EventsService delegate;

    public EventsDecorator(EventsService delegate) {
        this.delegate = delegate;
    }

    @Override
    public boolean getEventState(String event) {
        return this.delegate.getEventState(event);
    }

    @Override
    public void init() {
        this.delegate.init();
    }

    @Override
    public boolean putEventState(String event, boolean state) {
        return this.delegate.putEventState(event, state);
    }
}
