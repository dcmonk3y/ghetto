package org.ghetto.decorators;

import org.ghetto.services.CharacterService;
import org.ghetto.services.GameEngineService;
import org.ghetto.tools.Vec3;

public abstract class GameEngineDecorator implements GameEngineService {

    private final GameEngineService delegate;

    public GameEngineDecorator(GameEngineService delegate) {
        this.delegate = delegate;
    }

    @Override
    public boolean isEnd() {
        return this.delegate.isEnd();
    }

    @Override
    public boolean youHaveLost() {
        return this.delegate.youHaveLost();
    }

    @Override
    public boolean collision(CharacterService c1, CharacterService c2) {
        return this.delegate.collision(c1, c2);
    }

    @Override
    public void init() {
        this.delegate.init();
    }

    @Override
    public void update(int ms) {
        this.delegate.update(ms);
    }

    @Override
    public void render() {
        this.delegate.render();
    }

    @Override
    public void characterMove(CharacterService character, Vec3 dir) {
        this.delegate.characterMove(character, dir);
    }

    @Override
    public void characterBeat(CharacterService character) {
        this.delegate.characterBeat(character);
    }
}
