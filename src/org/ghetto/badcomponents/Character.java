package org.ghetto.badcomponents;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.ghetto.services.CharacterService;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Resource;
import org.ghetto.tools.Vec3;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Character implements CharacterService {

    private static HashMap<String, ArrayList<Animation>> ANIMATIONS;

    static {

        Character.ANIMATIONS = new HashMap<String, ArrayList<Animation>>();

        try {

            for (String name : new String[]{"alex", "slick", "villain"}) {

                ArrayList<SpriteSheet> sheets = new ArrayList<SpriteSheet>();

                sheets.add(new SpriteSheet(Resource.get("img/" + name
                        + "_idle.png"), 18, 32));
                sheets.add(new SpriteSheet(Resource.get("img/" + name
                        + "_walk.png"), 18, 32));
                sheets.add(new SpriteSheet(Resource.get("img/" + name
                        + "_hit.png"), 18, 32));
                sheets.add(new SpriteSheet(Resource.get("img/" + name
                        + "_beat.png"), 32, 32));
                sheets.add(new SpriteSheet(Resource.get("img/" + name
                        + "_dead.png"), 32, 18));

                ArrayList<Animation> list = new ArrayList<Animation>();

                for (SpriteSheet sheet : sheets) {

                    Animation original = new Animation(sheet, 60);
                    Animation back = new Animation();

                    for (int j = 0; j < original.getFrameCount(); j++) {
                        back.addFrame(
                                original.getImage(j)
                                .getFlippedCopy(true, false), 60);
                    }

                    list.add(original);
                    list.add(back);
                }

                Character.ANIMATIONS.put(name, list);
            }

        } catch (SlickException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    protected String m_name;
    protected Vec3 m_pos;
    protected Vec3 m_dir;
    protected Vec3 m_driftDir;
    protected float m_dim;
    protected float m_speed;
    protected int m_power;
    protected int m_freezeTime;
    protected int m_life;
    protected int m_money;
    protected boolean m_moving;
    protected boolean m_beating;
    protected ObjectService m_hands;

    public Character(String name, Vec3 pos, float dim, float speed, int life,
            int power) {

        this.init(name, pos, dim, speed, life, power);
    }

    // observers
    @Override
    public String getName() {

        return this.m_name;
    }

    @Override
    public Vec3 getPos() {

        return this.m_pos;
    }

    @Override
    public Vec3 getDir() {

        return this.m_dir;
    }

    @Override
    public Vec3 getDriftDir() {

        return this.m_driftDir;
    }

    @Override
    public float getDim() {

        return this.m_dim;
    }

    @Override
    public float getSpeed() {

        return this.m_speed;
    }

    @Override
    public int getPower() {

        int power = this.m_power;

        if (this.m_hands != null) {
            power += this.m_hands.getPower();
        }

        return power;
    }

    @Override
    public int getFreezeTime() {

        return this.m_freezeTime;
    }

    @Override
    public int getLife() {

        return this.m_life;
    }

    @Override
    public int getMoney() {

        return this.m_money;
    }

    @Override
    public boolean isDead() {

        return this.m_life == 2;
    }

    @Override
    public boolean isMoving() {

        return this.m_moving;
    }

    @Override
    public boolean isBeating() {

        return this.m_beating;
    }

    @Override
    public ObjectService getHands() {

        return this.m_hands;
    }

    // constructors
    @Override
    public void init(String name, Vec3 pos, float dim, float speed, int life,
            int power) {

        this.m_name = name;
        this.m_pos = new Vec3(pos);
        this.m_dim = dim;
        this.m_dir = new Vec3(1.f, 0.f, 0.f);
        this.m_dir = new Vec3();
        this.m_speed = speed;
        this.m_freezeTime = 0;
        this.m_power = power;
        this.m_life = life;
        this.m_money = 0;
        this.m_moving = false;
        this.m_beating = false;
        this.m_hands = null;
    }

    // operators
    @Override
    public void setPos(Vec3 pos) {

        this.m_pos = new Vec3(pos);
    }

    @Override
    public void addFreezeTime(int ms) {

        this.m_freezeTime += ms;
        //this.m_freezeTime = this.m_freezeTime < 0 ? 0 : this.m_freezeTime;
    }

    @Override
    public void addLife(int life) {

        this.m_life += life;
        this.m_life = this.m_life < 0 ? 0 : this.m_life;

        if(this.m_life == 0){
            this.stop();
        }
    }

    @Override
    public void addMoney(int money) {

        //this.m_money += money;
        this.m_money = this.m_money < 0 ? 0 : this.m_money;
    }

    @Override
    public void stop() {

        //this.m_moving = false;
        this.m_beating = false;
    }

    @Override
    public boolean drift(Vec3 dir) {

        if (this.isDead()) {
            return false;
        }

        this.m_moving = true;
        this.m_driftDir = dir;
        this.m_pos = Vec3.add(dir, this.m_pos);
        this.m_beating = false;
        return true;
    }

    @Override
    public boolean move(Vec3 dir) {

        if (this.m_freezeTime > 0 || this.isDead()) {
            return false;
        }

        this.m_moving = true;
        this.m_dir = dir;
        this.m_beating = false;
        //this.m_pos = Vec3.add(dir, this.m_pos);

        return true;
    }

    @Override
    public boolean beat() {

        if (this.m_freezeTime > 0 || this.isDead()) {
            return false;
        }

        this.m_moving = false;
        this.m_beating = true;
        this.addFreezeTime(300);

        return true;
    }

    @Override
    public boolean grab(ObjectService object) {

        if (this.m_freezeTime > 0 || this.m_hands != null || this.isDead()) {
            return false;
        }

        this.m_hands = object;

        return true;
    }

    @Override
    public boolean drop() {

        if (this.isDead()) {
            return false;
        }

        //this.m_hands = null;

        return true;
    }

    @Override
    public void render() {

        ArrayList<Animation> list = Character.ANIMATIONS.get(this.getName());

        int index = 0;

        if (this.isBeating()) {
            index = 2 * 3;
        }

        if (this.isMoving()) {
            index = 2 * 1;
            if (this.getFreezeTime() > 0) {
                index = 2 * 2;
            }
        }

        if (this.isDead()) {
            index = 2 * 4;
        }

        if (this.m_dir.x() < 0) {
            index++;
        }

        Animation anim = list.get(index);

        float x = this.getPos().x() - anim.getWidth() / 2.f;
        float y = -this.getPos().y() - anim.getHeight();

        anim.draw(x, y);
    }
}
