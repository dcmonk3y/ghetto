package org.ghetto.badcomponents;

import java.io.IOException;
import java.util.ArrayList;

import org.ghetto.services.BlockService;
import org.ghetto.services.BlockService.BLOCK_TYPE;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.services.RequireBlockService;
import org.ghetto.services.RequireObjectService;
import org.ghetto.tools.Resource;
import org.ghetto.tools.Vec3;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

public class Map implements MapService, RequireObjectService,
        RequireBlockService {

    public static final int BLOCK_MAP_POS_Y = 128;
    public static final int BLOCK_MAP_POS_X = 0;

    private int m_length;
    private int m_width;
    private int m_blockSize;
    private ArrayList<ObjectService> m_objects;
    private ArrayList<BlockService> m_blocks;
    private SpriteSheet sprite;

    public Map(int length, int width, int blockSize) {
        this.init(length, width, blockSize);
    }

    @Override
    public int getLength() {
        return this.m_length;
    }

    @Override
    public int getWidth() {
        return this.m_width;
    }

    @Override
    public int getBlockSize() {
        return this.m_blockSize;
    }

    @Override
    public BlockService getBlock(int l, int w) {

        // see getBlockAtPos to get block at vec pos

        /*
         * [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14]
         *
         * ex block 13 L = 5 w = 2 l = 3 5 * 2 = 10 10 + 3 = 13.
         */

        int pos = this.m_length * w + l;

        if (l >= this.m_length || l < 0)
            return null;
        if (w >= this.m_width || w < 0)
            return null;

        return this.m_blocks.get(pos);
    }

    @Override
    public BlockService getBlockAtPos(Vec3 pos) {

        float x = pos.x() / m_blockSize;
        float y = -pos.y() / m_blockSize;

        x = x < 0 ? -1 : x;
        y = y < 0 ? -1 : y;

        return this.getBlock((int) x, (int) y);
    }

    @Override
    public ArrayList<ObjectService> getObjects(Vec3 pos, float rad) {
        // return object from position with radius
        ArrayList<ObjectService> closeObjects = new ArrayList<ObjectService>();
        for (ObjectService ob : this.m_objects) {
            float dist = Vec3.distance(pos, ob.getPos());
            if (ob.isInMap() && dist < rad) {
                closeObjects.add(ob);
            }
        }
        return closeObjects;
    }
    
    @Override
    public ArrayList<ObjectService> getObjects() {
        return this.m_objects;
    }
    
    @Override
    public ArrayList<BlockService> getBlocks() {
        return this.m_blocks;
    }

    @Override
    public void init(int length, int width, int blockSize) {
        this.m_length = length;
        this.m_width = width;
        this.m_blockSize = blockSize;

        m_objects = new ArrayList<ObjectService>();
        m_blocks = new ArrayList<BlockService>();

        for(int i = 0; i < length * width; i++){
            m_blocks.add(null);
        }

        try {
            sprite = new SpriteSheet(Resource.get("img/bg1.png"), 1, 1);
        } catch (SlickException ex) {
            // Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null,
            // ex);
        } catch (IOException ex) {
            // Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null,
            // ex);
        }
    }

    @Override
    public void removeObject(ObjectService object) {
        object.setInMap(false);
    }

    @Override
    public void addObject(ObjectService object, Vec3 pos) {
        //object.setPos(pos);
        object.setInMap(true);
    }

    @Override
    public void bindObjectService(ObjectService object) {

        this.m_objects.add(object);
    }

    @Override
    public void bindBlockService(BlockService block) {

        int index = this.m_blocks.indexOf(null);

        if(index != -1){
            this.m_blocks.set(index, block);
        }
        else{
            System.out.println("aie");
        }
    }

    @Override
    public void render(Graphics g) {

        sprite.draw(0, 0, sprite.getWidth(), Map.BLOCK_MAP_POS_Y);

        // start blocks from map_height
        int x = BLOCK_MAP_POS_X, y = BLOCK_MAP_POS_Y;
        int i = 0;
        for (BlockService bl : this.m_blocks) {
            //
            Rectangle rect = new Rectangle(x, y, m_blockSize, m_blockSize);
            //
            if (bl.getType() == BLOCK_TYPE.EMPTY)
                g.fill(rect,
                        new GradientFill(m_blockSize / 2, 0, Color
                                .decode("#737573"), m_blockSize / 2,
                                m_blockSize, Color.decode("#737573"), true));
            else
                // change to draw to fill rectangle
                g.draw(rect,
                        new GradientFill(m_blockSize / 2, 0, Color
                                .decode("#737573"), m_blockSize / 2,
                                m_blockSize, Color.black, true));

            if (i < m_length - 1) {
                // increase x pos
                x += m_blockSize;
                // increase column
                i++;
            } else {
                // increase y pos
                y += m_blockSize;
                // reset x pos to start
                x = 0;
                // reset row
                i = 0;
            }
        }
    }
}
