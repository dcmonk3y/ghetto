package org.ghetto.badcomponents;

import java.util.HashMap;

import org.ghetto.services.EventsService;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;

public class Events implements EventsService, KeyListener, MouseListener {

    private Input m_input;
    private HashMap<String, Boolean> m_events;

    public Events() {

        this.init();
    }

    // observers

    @Override
    public boolean getEventState(String event) {

        if (this.m_events.keySet().contains(event) == false) {
            return false;
        }

        return this.m_events.get(event).booleanValue();
    }

    // constructors

    @Override
    public void init() {

        this.m_events = new HashMap<String, Boolean>();
        this.m_events.put("UP", false);
        this.m_events.put("DOWN", false);
        this.m_events.put("LEFT", false);
        this.m_events.put("RIGHT", false);
        this.m_events.put("BEAT", false);
        this.m_events.put("DROP", false);
        this.m_events.put("GRAB", false);
    }

    // operators

    @Override
    public boolean putEventState(String event, boolean state) {

        if (this.m_events.keySet().contains(event) == false) {
            return false;
        }

        this.m_events.put(event, state);
        return true;
    }

    // methods

    @Override
    public void inputEnded() {

    }

    @Override
    public void inputStarted() {

    }

    @Override
    public boolean isAcceptingInput() {

        return true;
    }

    @Override
    public void setInput(Input input) {

        this.m_input = input;
        this.m_input.addKeyListener(this);
        this.m_input.addMouseListener(this);
    }

    @Override
    public void keyPressed(int key, char c) {

        switch (c) {
        case 'z':
            this.m_events.put("UP", true);
            break;
        case 'w':
            this.m_events.put("UP", true);
            break;
        case 'a':
            this.m_events.put("LEFT", true);
            break;
        case 's':
            this.m_events.put("DOWN", true);
            break;
        case 'q':
            this.m_events.put("LEFT", true);
            break;
        case 'd':
            this.m_events.put("RIGHT", true);
            break;
        case ' ':
            this.m_events.put("BEAT", true);
            break;
        case 'e':
            this.m_events.put("DROP", true);
            break;
        case 'f':
            this.m_events.put("GRAB", true);
            break;
        }
    }

    @Override
    public void keyReleased(int key, char c) {

        switch (c) {
        case 'z':
            this.m_events.put("UP", false);
            break;
        case 'w':
            this.m_events.put("UP", false);
            break;
        case 'a':
            this.m_events.put("LEFT", false);
            break;
        case 's':
            this.m_events.put("DOWN", false);
            break;
        case 'q':
            this.m_events.put("LEFT", false);
            break;
        case 'd':
            this.m_events.put("RIGHT", false);
            break;
        case ' ':
            this.m_events.put("BEAT", false);
            break;
        case 'e':
            this.m_events.put("DROP", false);
            break;
        case 'f':
            this.m_events.put("GRAB", false);
            break;
        }
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {

    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {

    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {

    }

    @Override
    public void mousePressed(int button, int x, int y) {

    }

    @Override
    public void mouseReleased(int button, int x, int y) {

    }

    @Override
    public void mouseWheelMoved(int change) {

    }
}
