package org.ghetto.badcomponents;

import org.ghetto.services.BlockService;

public class Block implements BlockService {

    private BlockService.BLOCK_TYPE m_type;

    public Block(BLOCK_TYPE type) {
        this.init(type);
    }

    @Override
    public BLOCK_TYPE getType() {
        return this.m_type;
    }

    @Override
    public void init(BLOCK_TYPE type) {
        this.m_type = type;
    }
}
