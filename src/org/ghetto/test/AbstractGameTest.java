package org.ghetto.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.ghetto.contracts.ContractError;
import org.ghetto.contracts.PreconditionError;
import org.ghetto.services.BlockService;
import org.ghetto.services.BlockService.BLOCK_TYPE;
import org.ghetto.services.CharacterService;
import org.ghetto.services.EventsService;
import org.ghetto.services.GameEngineService;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.services.ObjectService.OBJECT_TYPE;
import org.ghetto.services.VillainService;
import org.ghetto.tools.Vec3;
import org.junit.Test;

public abstract class AbstractGameTest {

    public GameEngineService m_gameEngine;
    public EventsService m_events;
    public CharacterService m_character;
    public MapService m_map;
    public ArrayList<VillainService> m_villains;
    public ArrayList<BlockService> m_blocks;
    public ArrayList<ObjectService> m_objects;

    public AbstractGameTest() {

        this.m_gameEngine = null;
        this.m_events = null;
        this.m_map = null;
        this.m_character = null;
        this.m_villains = null;
        this.m_objects = null;
        this.m_blocks = null;
    }

    @Test
    public void checkCharacterInvariants() {

        assertTrue(this.m_character.getMoney() >= 0);
        assertTrue(this.m_character.getLife() >= 0);
        assertTrue(this.m_character.getDim() >= 0);
    }

    @Test
    public void checkMapInvariants() {

        assertTrue(this.m_map.getLength() >= 0);
        assertTrue(this.m_map.getWidth() >= 0);
        assertTrue(this.m_map.getBlockSize() >= 0);
        assertTrue(this.m_map.getBlocks().size() == this.m_map.getLength()
                * this.m_map.getWidth());
    }

    @Test
    public void checkObjectInvariants() {

        for (ObjectService obj : this.m_objects) {
            if (obj.getType() == ObjectService.OBJECT_TYPE.MONEY
                    || obj.getType() == ObjectService.OBJECT_TYPE.LIFE) {
                assertTrue(obj.isEquipable() == false);
            }
            if (obj.getType() == ObjectService.OBJECT_TYPE.BATTON
                    || obj.getType() == ObjectService.OBJECT_TYPE.CHAIN
                    || obj.getType() == ObjectService.OBJECT_TYPE.BIN) {
                assertTrue(obj.isEquipable());
            }
        }
    }

    @Test
    public void characterTestInit() {

        try {
            this.m_character.init("alex", new Vec3(10.f, 0.f, 0.f), 7.f, 100.f,
                    10, 2);
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestInitPreNegativeName() {

        try {
            this.m_character.init("", new Vec3(10.f, 0.f, 0.f), 7.f, 100.f, 10,
                    2);
            assertTrue("No exception on Name", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestInitPreNegativeDim() {

        try {
            this.m_character.init("alex", new Vec3(10.f, 0.f, 0.f), -7.f,
                    100.f, 10, 2);
            assertTrue("No exception on Dim", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestInitPreNegativeSpeed() {

        try {
            this.m_character.init("alex", new Vec3(10.f, 0.f, 0.f), 7.f,
                    -100.f, 10, 2);
            assertTrue("No exception on Speed", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestInitPreNegativeLife() {

        try {
            this.m_character.init("alex", new Vec3(10.f, 0.f, 0.f), 7.f, 100.f,
                    -10, 2);
            assertTrue("No exception on Life", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestInitPreNegativePower() {

        try {
            this.m_character.init("alex", new Vec3(10.f, 0.f, 0.f), 7.f, 100.f,
                    10, -2);
            assertTrue("No exception on Power", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestAddFreezeTime() {

        try {
            this.m_character.addFreezeTime(10);
            this.m_character.addFreezeTime(-11);
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestAddLife() {

        try {
            this.m_character.addLife(10);
            this.m_character.addLife(-11);
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestAddMoney() {

        try {
            this.m_character.addMoney(10);
            this.m_character.addMoney(-11);
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestSetPos() {

        try {
            this.m_character.setPos(new Vec3(100.f, -100.f, 0.f));
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestStop() {

        try {
            this.m_character.move(new Vec3(10.f, -10.f, 0.f));
            this.m_character.stop();
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestDrop() {

        try {
            this.m_character.grab(this.m_map.getObjects().get(0));
            this.m_character.drop();
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestDropPreNegativeNull() {

        try {
            this.m_character.drop();
            assertTrue("No exception on getHands() == null", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestDropPreNegativeDead() {

        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_character.drop();
            assertTrue("No exception on isDead()", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestDropPreNegativeFrozen() {

        try {
            this.m_character.addFreezeTime(10);
            this.m_character.drop();
            assertTrue("No exception on getFreezetime() != 0", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestGrab() {

        try {
            this.m_character.grab(this.m_map.getObjects().get(0));
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestGrabPreNegativeNull() {

        try {
            this.m_character.grab(this.m_map.getObjects().get(0));
            this.m_character.grab(this.m_map.getObjects().get(0));
            assertTrue("No exception on getHands() != null", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestGrabPreNegativeDead() {

        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_character.grab(this.m_map.getObjects().get(0));
            assertTrue("No exception on isDead()", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestGrabPreNegativeFrozen() {

        try {
            this.m_character.addFreezeTime(10);
            this.m_character.grab(this.m_map.getObjects().get(0));
            assertTrue("No exception on getFreezetime() != 0", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestBeat() {

        try {
            this.m_character.beat();
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestBeatPreNegativeDead() {

        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_character.beat();
            assertTrue("No exception on isDead()", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestBeatPreNegativeFrozen() {

        try {
            this.m_character.addFreezeTime(10);
            this.m_character.beat();
            assertTrue("No exception on getFreezetime() != 0", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestDrift() {

        try {
            this.m_character.drift(new Vec3(10.f, -10.f, 0.f));
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestDriftPreNegativeDead() {

        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_character.drift(new Vec3(10.f, -10.f, 0.f));
            assertTrue("No exception on isDead()", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestMove() {

        try {
            this.m_character.move(new Vec3(10.f, -10.f, 0.f));
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void characterTestMovePreNegativeDead() {

        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_character.move(new Vec3(10.f, -10.f, 0.f));
            assertTrue("No exception on isDead()", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void characterTestMovePreNegativeFrozen() {

        try {
            this.m_character.addFreezeTime(10);
            this.m_character.move(new Vec3(10.f, -10.f, 0.f));
            assertTrue("No exception on getFreezetime() != 0", false);
        } catch (ContractError e) {

        }
    }

    @Test
    public void mapTestInit() {

        try {
            this.m_map.init(10, 5, 5);
            System.out.println(this.m_map.getBlocks().size());
        } catch (ContractError e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestInitPreNegativeLength() {

        try {
            this.m_map.init(-10, 5, 5);
            assertTrue("No exception on length", false);
        } catch (PreconditionError e) {

        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestInitPreNegativeWidth() {

        try {
            this.m_map.init(10, -5, 5);
            assertTrue("No exception on width", false);
        } catch (PreconditionError e) {

        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestInitPreNegativeBlockSize() {

        try {
            this.m_map.init(10, 5, -1);
            assertTrue("No exception on blockSize", false);
        } catch (PreconditionError e) {

        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestRemoveObject() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            this.m_map.removeObject(obj);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestRemoveObjectPreNegativeIsInMap() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.setInMap(false);
            this.m_map.removeObject(obj);
            assertTrue("No exception on IsInMap() == false", false);
        } catch (PreconditionError e) {

        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void mapTestAddObject() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.setInMap(false);
            this.m_map.addObject(obj, new Vec3());
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }

    }

    @Test
    public void mapTestAddObjectPreNegativeIsInMap() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.setInMap(true);
            this.m_map.addObject(obj, new Vec3());
            assertTrue("No exception on IsInMap() == true", false);
        } catch (PreconditionError e) {

        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void objectTestInit() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.init(OBJECT_TYPE.BATTON, new Vec3());
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void objectTestSetPos() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.setPos(new Vec3(100.f, 100.f, 0.f));
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void objectTestSetInMap() {

        try {
            ObjectService obj = this.m_map.getObjects().get(0);
            obj.setInMap(false);
            obj.setInMap(true);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void blockTestInit() {

        try {
            BlockService block = this.m_map.getBlocks().get(0);
            block.init(BLOCK_TYPE.EMPTY);
            block.init(BLOCK_TYPE.HOLE);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestInit() {
        try {
            this.m_gameEngine.init();
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestCharacterBeat() {
        try {
            this.m_gameEngine.characterBeat(m_character);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestCharacterBeatVillain() {
        try {
            VillainService v = this.m_villains.get(0);
            v.setPos(Vec3.sub(m_character.getPos(), new Vec3(5, 5, 0)));
            this.m_gameEngine.characterBeat(m_character);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestVillainBeatCharacter() {
        try {
            VillainService v = this.m_villains.get(0);
            v.setPos(Vec3.sub(m_character.getPos(), new Vec3(5, 5, 0)));
            this.m_gameEngine.characterBeat(v);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestCharacterBeatPreNegativeDead() {
        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_gameEngine.characterBeat(m_character);
            assertTrue("No exception on isDead()", false);
        } catch (Error e) {
        }
    }
    
    @Test
    public void gameEngineTestCharacterMove() {
        try {
            this.m_gameEngine.characterMove(m_character, new Vec3(1,1,0));
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
    @Test
    public void gameEngineTestCharacterMovePreNegativeDead() {
        try {
            this.m_character.addLife(-this.m_character.getLife());
            this.m_gameEngine.characterMove(m_character, new Vec3(1,1,0));
            assertTrue("No exception on isDead()", false);
        } catch (Error e) {
        }
    }
    
    @Test
    public void gameEngineTestUpdate() {
        try {
            //update 300 ms
            this.m_gameEngine.update(300);
        } catch (Error e) {
            assertTrue(e.toString(), false);
        }
    }
    
}
