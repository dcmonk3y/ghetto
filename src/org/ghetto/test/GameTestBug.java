package org.ghetto.test;

import org.ghetto.game.GameBug;
import org.junit.After;
import org.junit.Before;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

public class GameTestBug extends AbstractGameTest {

    protected GameBug m_game;

    public GameTestBug() {

        this.m_game = new GameBug();
    }
     
    @Before
    public void beforeTests() {

        try {
            Display.create();
        } catch (LWJGLException e) {

        }

        this.m_game.initComponents();
        this.m_game.initServices(true);
        this.m_game.bindComponents();

        this.m_gameEngine = this.m_game.m_gameEngine;
        this.m_events = this.m_game.m_events;
        this.m_map = this.m_game.m_map;
        this.m_character = this.m_game.m_character;
        this.m_villains = this.m_game.m_villains;
        this.m_objects = this.m_game.m_objects;
        this.m_blocks = this.m_game.m_blocks;
    }

    @After
    public final void afterTests() {

        this.m_gameEngine = null;
        this.m_events = null;
        this.m_map = null;
        this.m_character = null;
        this.m_villains = null;
        this.m_objects = null;
        this.m_blocks = null;

        Display.destroy();
    }
}
