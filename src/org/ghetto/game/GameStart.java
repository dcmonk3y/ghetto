package org.ghetto.game;

import java.awt.Font;

import org.ghetto.components.Map;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Resource;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

public class GameStart extends BasicGame {

    private static boolean EMBEDDED_TEST = false;

    private GameImpl m_game;
    private TrueTypeFont m_ttf;

    public GameStart(String name) {

        super(name);

        this.m_game = null;
        this.m_ttf = null;
    }

    @Override
    public void init(GameContainer gc) throws SlickException {

        // game creation
        this.m_game = new GameImpl();

        // components creation
        this.m_game.initComponents();
        this.m_game.initServices(GameStart.EMBEDDED_TEST);

        // components binding
        this.m_game.bindComponents();

        // special tricks
        this.m_game.m_eventsComponent.setInput(gc.getInput());
        Music openingMenuMusic = new Music(Resource.get("rcr-maintheme.ogg"));
        openingMenuMusic.loop();
        Font font = new Font("Verdana", Font.BOLD, 12);
        this.m_ttf = new TrueTypeFont(font, true);
    }

    @Override
    public void update(GameContainer gc, int ms) throws SlickException {

        if (this.m_game.m_gameEngine.isEnd()) {
            return;
        }

        int elapsedTime = (ms > GameImpl.LAG_TIME) ? GameImpl.LAG_TIME : ms;

        while (elapsedTime > 0) {
            int time = elapsedTime > GameImpl.STEP_TIME ? GameImpl.STEP_TIME
                    : elapsedTime;
            elapsedTime -= GameImpl.STEP_TIME;
            this.m_game.m_gameEngine.update(time);
        }
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {

        this.m_game.m_map.render(g);

        g.pushTransform();
        g.translate(Map.BLOCK_MAP_POS_X, Map.BLOCK_MAP_POS_Y);
        this.m_game.m_gameEngine.render();
        g.popTransform();

        String hub = "";
        hub += " Life: " + Integer.toString(this.m_game.m_character.getLife());
        hub += " Money: "
                + Integer.toString(this.m_game.m_character.getMoney());
        ObjectService hands = this.m_game.m_character.getHands();
        if (hands == null) {
            hub += " Hands: ("
                    + Integer.toString(this.m_game.m_character.getPower())
                    + ")";
        } else {
            hub += " Hands: ("
                    + Integer.toString(this.m_game.m_character.getPower())
                    + ") " + hands.getType();
        }

        this.m_ttf.drawString(5.f, GameImpl.WIN_HEIGHT - m_ttf.getLineHeight()
                - 5.f, hub, Color.white);

        if (this.m_game.m_gameEngine.isEnd()) {
            g.setColor(new Color(0.f, 0.f, 0.f, 0.8f));
            g.fillRect(0.f, 0.f, GameImpl.WIN_WIDTH, GameImpl.WIN_HEIGHT);
            String endString = "YOU WIN !!!";
            if (this.m_game.m_gameEngine.youHaveLost()) {
                endString = "YOU LOSE...";
            }
            int x = (GameImpl.WIN_WIDTH - this.m_ttf.getWidth(endString)) / 2;
            int y = GameImpl.WIN_HEIGHT / 2;
            this.m_ttf.drawString(x, y, endString, Color.white);
        }
    }

    public static void main(String[] args) {

        for (String arg : args) {
            if (arg.equals("-embeddedTest")) {
                GameStart.EMBEDDED_TEST = true;
            }
        }

        try {
            AppGameContainer appgc;
            appgc = new AppGameContainer(new GameStart("ghetto"));
            appgc.setDisplayMode(GameImpl.WIN_WIDTH, GameImpl.WIN_HEIGHT, false);
            appgc.start();
        } catch (SlickException ex) {

        }
    }
}
