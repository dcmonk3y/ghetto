package org.ghetto.game;

import java.util.ArrayList;

import org.ghetto.components.Block;
import org.ghetto.components.Character;
import org.ghetto.components.Events;
import org.ghetto.components.GameEngine;
import org.ghetto.components.Map;
import org.ghetto.components.Object;
import org.ghetto.components.Villain;
import org.ghetto.contracts.BlockContract;
import org.ghetto.contracts.CharacterContract;
import org.ghetto.contracts.EventsContract;
import org.ghetto.contracts.GameEngineContract;
import org.ghetto.contracts.MapContract;
import org.ghetto.contracts.ObjectContract;
import org.ghetto.contracts.VillainContract;
import org.ghetto.services.BlockService;
import org.ghetto.services.CharacterService;
import org.ghetto.services.EventsService;
import org.ghetto.services.GameEngineService;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.services.VillainService;
import org.ghetto.tools.BlockFactory;
import org.ghetto.tools.ObjectFactory;
import org.ghetto.tools.Vec3;
import org.ghetto.tools.VillainFactory;

public class GameImpl {

    public static final int WIN_WIDTH = 760;
    public static final int WIN_HEIGHT = 480;
    public static final int STEP_TIME = 10;
    public static final int LAG_TIME = 60;
    public static final int BLOCK_COUNT_WIDTH = 8;
    public static final int BLOCK_COUNT_LENGTH = 19;
    public static final int BLOCK_SIZE = 40;

    public GameEngine m_gameEngineComponent;
    public Events m_eventsComponent;
    public Character m_characterComponent;
    public Map m_mapComponent;
    public ArrayList<Villain> m_villainsComponent;
    public ArrayList<Block> m_blocksComponent;
    public ArrayList<Object> m_objectsComponent;

    public GameEngineService m_gameEngine;
    public EventsService m_events;
    public CharacterService m_character;
    public MapService m_map;
    public ArrayList<VillainService> m_villains;
    public ArrayList<BlockService> m_blocks;
    public ArrayList<ObjectService> m_objects;

    public GameImpl() {

        // components
        this.m_gameEngineComponent = null;
        this.m_eventsComponent = null;
        this.m_mapComponent = null;
        this.m_characterComponent = null;
        this.m_villainsComponent = null;
        this.m_objectsComponent = null;
        this.m_blocksComponent = null;

        // services
        this.m_gameEngine = null;
        this.m_events = null;
        this.m_map = null;
        this.m_character = null;
        this.m_villains = null;
        this.m_objects = null;
        this.m_blocks = null;
    }

    public void initComponents() {

        this.m_gameEngineComponent = new GameEngine();
        this.m_mapComponent = new Map(BLOCK_COUNT_LENGTH, BLOCK_COUNT_WIDTH,
                BLOCK_SIZE);
        this.m_eventsComponent = new Events();
        this.m_characterComponent = new Character("alex", new Vec3(10.f, 0.f,
                0.f), 7.f, 100.f, 10, 2);
        this.m_villainsComponent = VillainFactory.createVillains();
        this.m_objectsComponent = ObjectFactory.createObjects();
        this.m_blocksComponent = BlockFactory.createBlocks(BLOCK_COUNT_LENGTH
                * BLOCK_COUNT_WIDTH);
    }

    public void initServices(boolean embeddedTest) {

        this.m_villains = new ArrayList<VillainService>();
        this.m_objects = new ArrayList<ObjectService>();
        this.m_blocks = new ArrayList<BlockService>();

        if (embeddedTest) {
            this.m_gameEngine = new GameEngineContract(
                    this.m_gameEngineComponent);
            this.m_map = new MapContract(this.m_mapComponent);
            this.m_events = new EventsContract(this.m_eventsComponent);
            this.m_character = new CharacterContract(this.m_characterComponent);
            for (VillainService villain : this.m_villainsComponent) {
                this.m_villains.add(new VillainContract(villain));
            }
            for (ObjectService object : this.m_objectsComponent) {
                this.m_objects.add(new ObjectContract(object));
            }
            for (BlockService block : this.m_blocksComponent) {
                this.m_blocks.add(new BlockContract(block));
            }
        } else {
            this.m_gameEngine = this.m_gameEngineComponent;
            this.m_map = this.m_mapComponent;
            this.m_events = this.m_eventsComponent;
            this.m_character = this.m_characterComponent;
            for (VillainService villain : this.m_villainsComponent) {
                this.m_villains.add(villain);
            }
            for (ObjectService object : this.m_objectsComponent) {
                this.m_objects.add(object);
            }
            for (BlockService block : this.m_blocksComponent) {
                this.m_blocks.add(block);
            }
        }
    }

    public void bindComponents() {

        this.m_gameEngineComponent.bindEventsService(this.m_events);
        this.m_gameEngineComponent.bindMapService(this.m_map);
        this.m_gameEngineComponent.bindCharacterService(this.m_character);
        this.m_gameEngineComponent.bindDrawableService(this.m_character);

        for (Villain villain : this.m_villainsComponent) {
            villain.bindCharacterService(this.m_character);
            villain.bindGameEngineService(this.m_gameEngine);
        }

        for (VillainService villain : this.m_villains) {
            this.m_gameEngineComponent.bindVillainService(villain);
            this.m_gameEngineComponent.bindDrawableService(villain);
        }

        for (ObjectService object : this.m_objects) {
            this.m_gameEngineComponent.bindDrawableService(object);
            this.m_mapComponent.bindObjectService(object);
        }

        for (BlockService block : this.m_blocks) {
            this.m_mapComponent.bindBlockService(block);
        }
    }
}
