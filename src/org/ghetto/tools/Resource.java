package org.ghetto.tools;

import java.net.URL;

public class Resource {

    public final static String PREFIX = "res/";

    public static URL get(String s) {

        String search = Resource.PREFIX + s;
        return Resource.class.getClassLoader().getResource(search);
    }
}
