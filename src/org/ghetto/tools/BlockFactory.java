package org.ghetto.tools;

import java.util.ArrayList;

import org.ghetto.components.Block;
import org.ghetto.services.BlockService.BLOCK_TYPE;

public final class BlockFactory {

    public static ArrayList<Block> createBlocks(int number_blocks) {

        ArrayList<Block> list = new ArrayList<Block>();

        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.HOLE));
        list.add(new Block(BLOCK_TYPE.HOLE));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.HOLE));
        list.add(new Block(BLOCK_TYPE.EMPTY));
        list.add(new Block(BLOCK_TYPE.EMPTY));

        while (list.size() < number_blocks) {

            list.add(new Block(BLOCK_TYPE.EMPTY));
        }

        return list;
    }
}
