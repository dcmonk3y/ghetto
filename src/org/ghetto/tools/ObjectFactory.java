package org.ghetto.tools;

import java.util.ArrayList;

import org.ghetto.components.Object;
import org.ghetto.services.ObjectService.OBJECT_TYPE;

public final class ObjectFactory {

    public static ArrayList<Object> createObjects() {

        ArrayList<Object> list = new ArrayList<Object>();

        list.add(new Object(OBJECT_TYPE.MONEY, new Vec3(300.f, -170.f, 0.f)));
        list.add(new Object(OBJECT_TYPE.CHAIN, new Vec3(200.f, -170.f, 0.f)));
        list.add(new Object(OBJECT_TYPE.BATTON, new Vec3(400.f, -170.f, 0.f)));
        list.add(new Object(OBJECT_TYPE.MONEY, new Vec3(100.f, -170.f, 0.f)));

        return list;
    }
}
