package org.ghetto.tools;

import java.util.ArrayList;

import org.ghetto.badcomponents.Villain;

public final class BadVillainFactory {

    public static ArrayList<Villain> createVillains() {

        ArrayList<Villain> list = new ArrayList<Villain>();

        list.add(new Villain("slick", new Vec3(250.f, -100.f, 0.f), 7.f, 75.f,
                10, 3));
        list.add(new Villain("villain", new Vec3(150.f, -170.f, 0.f), 7.f,
                75.f, 5, 1));
        list.add(new Villain("villain", new Vec3(250.f, -170.f, 0.f), 7.f,
                75.f, 5, 1));
        list.add(new Villain("villain", new Vec3(350.f, -170.f, 0.f), 7.f,
                75.f, 5, 1));
        list.add(new Villain("villain", new Vec3(450.f, -170.f, 0.f), 7.f,
                75.f, 5, 1));

        return list;
    }
}
