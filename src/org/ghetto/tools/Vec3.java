package org.ghetto.tools;

public final class Vec3 {

    public final static float EPSILON = 0.00001f;

    private float m_x;
    private float m_y;
    private float m_z;

    public Vec3() {

        this.m_x = 0.f;
        this.m_y = 0.f;
        this.m_z = 0.f;
    }

    public Vec3(float x, float y, float z) {

        this.m_x = x;
        this.m_y = y;
        this.m_z = z;
    }

    public Vec3(Vec3 v) {

        this.m_x = v.m_x;
        this.m_y = v.m_y;
        this.m_z = v.m_z;
    }

    public String toString() {

        return Float.toString(this.m_x) + " " + Float.toString(this.m_y) + " "
                + Float.toString(this.m_z);
    }

    public float x() {

        return this.m_x;
    }

    public float y() {

        return this.m_y;
    }

    public float z() {

        return this.m_z;
    }

    public static Vec3 add(Vec3 v1, Vec3 v2) {

        return new Vec3(v1.m_x + v2.m_x, v1.m_y + v2.m_y, v1.m_z + v2.m_z);
    }

    public static Vec3 sub(Vec3 v1, Vec3 v2) {

        return new Vec3(v1.m_x - v2.m_x, v1.m_y - v2.m_y, v1.m_z - v2.m_z);
    }

    public static Vec3 scale(Vec3 v, float n) {

        return new Vec3(v.m_x * n, v.m_y * n, v.m_z * n);
    }

    public static float norm(Vec3 v) {

        float tmp = v.m_x * v.m_x + v.m_y * v.m_y + v.m_z * v.m_z;
        return (float) Math.sqrt(tmp);
    }

    public static Vec3 normalize(Vec3 v) {

        float n = Vec3.norm(v);

        if (n < Vec3.EPSILON)
            return v;

        return Vec3.scale(v, 1.f / n);
    }

    public static float dot(Vec3 v1, Vec3 v2) {

        return (v1.m_x * v2.m_x + v1.m_y * v2.m_y + v1.m_z * v2.m_z);
    }

    public static float distance(Vec3 v1, Vec3 v2) {

        return Vec3.norm(Vec3.sub(v2, v1));
    }
}
