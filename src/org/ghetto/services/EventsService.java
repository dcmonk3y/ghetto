package org.ghetto.services;

public interface EventsService {

    // observators

    public boolean getEventState(String event);

    // end observators

    // constructors

    public void init();

    // end constructors

    // operators

    // this operator is used only for tests
    public boolean putEventState(String event, boolean state);

    // end operators
}
