package org.ghetto.services;

public interface RequireObjectService {

    public void bindObjectService(ObjectService object);
}
