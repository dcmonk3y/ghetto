package org.ghetto.services;

public interface BlockService {

    enum BLOCK_TYPE {
        EMPTY, HOLE
    };

    // observators

    public BLOCK_TYPE getType();

    // end observators

    // invariants

    // end invariants

    // constructors

    // post: getType(B) == type
    public void init(BLOCK_TYPE type);

    // end constructors
}
