package org.ghetto.services;

public interface VillainService extends CharacterService {

    // operators

    public void think(int ms);

    // end operators
}
