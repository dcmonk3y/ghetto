package org.ghetto.services;

import org.ghetto.tools.Vec3;

public interface CharacterService extends DrawableService {

    // observators

    public String getName();

    public Vec3 getDir();

    public Vec3 getDriftDir();

    public float getDim();

    public float getSpeed();

    public int getPower();

    public int getFreezeTime();

    public int getLife();

    public int getMoney();

    public boolean isDead();

    public boolean isMoving();

    public boolean isBeating();

    public ObjectService getHands();

    // end observators

    // invariants

    // inv: getMoney(C) >= 0
    // inv: getLife(C)) >= 0
    // inv: getDim(C)) >= 0

    // end invariants

    // constructors

    // pre: life >= 0
    // pre: power >= 0
    // pre: name != null || name != ""
    // pre: dim > 0
    // pre: speed > 0
    // post: getLife(C) == life
    // post: getPower(C) == power
    // post: getName(C) == name
    // post: getPos(C) == pos
    // post: getDim(C) == dim
    // post: getSpeed(C) == speed
    // post: getHands(C) == null
    public void init(String name, Vec3 pos, float dim, float speed, int life,
            int power);

    // end constructors

    // operators

    // post: getPos(C) == pos
    public void setPos(Vec3 pos);

    // post: getFreezeTime(C) == max(getFreezeTime(C)@pre + ms, 0)
    public void addFreezeTime(int ms);

    // post: getLife(C) == max(getLife(C)@pre + life, 0)
    public void addLife(int life);

    // post: getMoney(C) == max(getMoney(C)@pre + cash, 0)
    public void addMoney(int cash);

    // post: isMoving(C) == false
    // post: isBeating(C) == false
    public void stop();

    // pre: isDead(C) == false
    // post: isMoving(C) == true
    // post: getDriftDir(C) == dir
    // post: getPos(C) == dir + getPos(C)@pre
    public boolean drift(Vec3 dir);

    // pre: isDead(C) == false
    // pre: getFreezetime(C) == 0
    // post: isMoving(C) == true
    // post: getDir(C) == dir
    // post: getPos(C) == dir + getPos(C)@pre
    public boolean move(Vec3 dir);

    // pre: isDead(C) == false
    // pre: getFreezetime(C) == 0
    // post: isMoving(C) == false
    // post: isBeating(C) == true
    public boolean beat();

    // pre: getHands(C) == null
    // pre: isDead(C) == false
    // pre: getFreezetime(C) == 0
    // post: getHands(C) == object
    public boolean grab(ObjectService object);

    // pre: getHands(C) != null
    // pre: isDead(C) == false
    // pre: getFreezetime(C) == 0
    // post: getHands(C) == null
    public boolean drop();

    // end operators
}
