package org.ghetto.services;

import java.util.ArrayList;

import org.ghetto.tools.Vec3;
import org.newdawn.slick.Graphics;

public interface MapService {

    // observators

    public int getLength();

    public int getWidth();

    public int getBlockSize();

    public BlockService getBlock(int l, int w);

    public BlockService getBlockAtPos(Vec3 pos);

    public ArrayList<ObjectService> getObjects(Vec3 pos, float rad);

    public ArrayList<ObjectService> getObjects();

    public ArrayList<BlockService> getBlocks();

    // end observators

    // invariants

    // inv: getLength(M) >= 0
    // inv: getWidth(M) >= 0
    // inv: getBlockSize(M) >= 0
    // inv: |getBlocks(M)| == getLength(M) * getWidth(M)

    // end invariants

    // constructors

    // pre: length >= 0
    // pre: width >= 0
    // pre: blockSize > 0
    // post: getWidth(M) == width
    // post: getLength(M) == length
    // post: getBlockSize(M) == blockSize
    public void init(int length, int width, int blockSize);

    // end constructors

    // operators

    // pre: isInMap(object) == true
    // post: isInMap(object) == false
    public void removeObject(ObjectService object);

    // pre: isInMap(object) == false
    // post: isInMap(object) == true
    // post: getPos(object) == pos
    public void addObject(ObjectService object, Vec3 pos);

    public void render(Graphics g);

    // end operators
}
