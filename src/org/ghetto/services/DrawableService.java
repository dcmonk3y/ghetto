package org.ghetto.services;

import org.ghetto.tools.Vec3;

public interface DrawableService {

    // observators

    public Vec3 getPos();

    // end observators

    // operators

    public void render();

    // end operators
}
