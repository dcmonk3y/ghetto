package org.ghetto.services;

import org.ghetto.tools.Vec3;

public interface GameEngineService {

    // observators

    public boolean isEnd();

    public boolean youHaveLost();
    
    public boolean collision(CharacterService c1, CharacterService c2);

    // end observators

    // invariants

    // end invariants

    // constructors

    public void init();

    // end constructors

    // operators

    public void update(int ms);

    public void render();

    public void characterMove(CharacterService character, Vec3 dir);

    public void characterBeat(CharacterService character);

    // end operators
}
