package org.ghetto.services;

public interface RequireGameEngineService {

    public void bindGameEngineService(GameEngineService block);
}
