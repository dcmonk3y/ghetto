package org.ghetto.services;

public interface RequireDrawableService {

    public void bindDrawableService(DrawableService drawable);
}
