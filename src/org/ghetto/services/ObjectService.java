package org.ghetto.services;

import org.ghetto.tools.Vec3;

public interface ObjectService extends DrawableService {

    public enum OBJECT_TYPE {
        MONEY, CHAIN, BATTON, BIN, LIFE
    };

    // observators

    public OBJECT_TYPE getType();

    public boolean isEquipable();

    public int getPower();

    public boolean isInMap();

    // end observators

    // invariants

    // inv: if getType(O) in [MONEY, LIFE] then isEquipable(O) == false
    // inv: if getType(O) in [BATTON, CHAIN, BIN] then isEquipable(O) == true

    // end invariants

    // constructors

    // post: getType(O) == type
    // post: getPos(O) == pos
    public void init(OBJECT_TYPE type, Vec3 pos);

    // end constructors

    // operators

    // post: getPos(O) == pos
    public void setPos(Vec3 pos);

    // post: isInMap(O) == in
    public void setInMap(boolean in);

    // end operators
}
