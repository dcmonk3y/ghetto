package org.ghetto.contracts;

import org.ghetto.decorators.GameEngineDecorator;
import org.ghetto.services.CharacterService;
import org.ghetto.services.GameEngineService;
import org.ghetto.tools.Vec3;

public class GameEngineContract extends GameEngineDecorator {

    public GameEngineContract(GameEngineService delegate) {
        super(delegate);
    }

    public void checkInvariant() {
        // inv : if youHaveLost()
        // then isEnd() == true
        if (youHaveLost()) {
            if (!isEnd()) {
                throw new InvariantError(
                        "The game must have ended if the player has lost");
            }
        }
    }

    @Override
    public void characterBeat(CharacterService character) {

        checkInvariant();

        super.characterBeat(character);

        checkInvariant();
    }

    @Override
    public void characterMove(CharacterService character, Vec3 dir) {

        checkInvariant();

        super.characterMove(character, dir);

        checkInvariant();
    }

    @Override
    public void update(int ms) {

        // //pre : ms > 0
        // if (ms <= 0) {
        // throw new
        // PreconditionError("Update value ms is less than value minimum");
        // }

        checkInvariant();

        super.update(ms);

        checkInvariant();
    }

    @Override
    public void init() {

        checkInvariant();

        super.init();

        checkInvariant();
    }

    @Override
    public void render() {

        checkInvariant();

        super.render();

        checkInvariant();
    }
}
