package org.ghetto.contracts;

import org.ghetto.decorators.MapDecorator;
import org.ghetto.services.MapService;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Vec3;

public class MapContract extends MapDecorator {

    public MapContract(MapService delegate) {
        super(delegate);
    }

    public void checkInvariant() {

        // inv: getLength(M) >= 0
        if (getLength() < 0) {
            throw new InvariantError(
                    "getLength(M) < 0");
        }

        // inv: getWidth(M) >= 0
        if (getWidth() < 0) {
            throw new InvariantError(
                    "getWidth(M) < 0");
        }

        // inv: getBlockSize(M) > 0
        if (getBlockSize() <= 0) {
            throw new InvariantError(
                    "getBlockSize(M) <= 0");
        }
        
        // inv: |getBlocks(M)| == getLength(M) * getWidth(M)
        if (getBlocks().size() != getLength() * getWidth()) {
            throw new InvariantError(
                    "|getBlocks(M)| != getLength(M) * getWidth(M)");
        }

    }

    @Override
    public void init(int length, int width, int blockSize) {

        // pre: length >= 0
        if (!(length > 0)) {
            throw new PreconditionError(
                    "Map length is less than minimum allowed");
        }

        // pre: width >= 0
        if (!(width > 0)) {
            throw new PreconditionError(
                    "Map width is less than minimum allowed");
        }

        // pre: blockSize > 0
        if (!(blockSize > 0)) {
            throw new PreconditionError(
                    "Blocksize is less than minimum allowed");
        }

        checkInvariant();

        super.init(length, width, blockSize);

        checkInvariant();

        // post: getWidth(M) == width
        if (!(getWidth() == width)) {
            throw new PostconditionError("Width not correctly set");
        }

        // post: getLength(M) == length
        if (!(getLength() == length)) {
            throw new PostconditionError("Length not correctly set");
        }

        // post: getBlockSize(M) == blockSize
        if (!(getBlockSize() == blockSize)) {
            throw new PostconditionError("Blocksize not correctly set");
        }
    }

    @Override
    public void removeObject(ObjectService object) {

        // pre: isInMap(object) == true
        if (!object.isInMap()) {
            throw new PreconditionError("Object is not in map");
        }

        checkInvariant();

        super.removeObject(object);

        checkInvariant();

        // post: isInMap(object) == false
        if (object.isInMap()) {
            throw new PostconditionError(
                    "Object has not been successfully removed");
        }
    }

    @Override
    public void addObject(ObjectService object, Vec3 pos) {

        // pre: isInMap(object) == false
        if (object.isInMap()) {
            throw new PreconditionError("Object is already in map");
        }

        checkInvariant();

        super.addObject(object, pos);

        checkInvariant();

        // post: isInMap(object) == true
        if (!object.isInMap()) {
            throw new PostconditionError(
                    "Object has not been successfully added");
        }

        // post: getPos(object) == pos
        if (Vec3.distance(object.getPos(), pos) > Vec3.EPSILON) {
            throw new PostconditionError("Object position not successfully set");
        }
    }
}
