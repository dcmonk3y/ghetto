package org.ghetto.contracts;

import org.ghetto.decorators.ObjectDecorator;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Vec3;

public class ObjectContract extends ObjectDecorator {

    public ObjectContract(ObjectService delegate) {
        super(delegate);
    }

    public void checkInvariant() {

        // inv: if getType(O) in [MONEY, LIFE] then isEquipable(O) == false
        if (getType() == ObjectService.OBJECT_TYPE.MONEY
                || getType() == ObjectService.OBJECT_TYPE.LIFE) {
            if (isEquipable()) {
                throw new InvariantError("Money cannot be equiped");
            }
        }

        // inv: if getType(O) in [BATTON, CHAIN, BIN] then isEquipable(O) ==
        // true
        if (getType() == ObjectService.OBJECT_TYPE.BATTON
                || getType() == ObjectService.OBJECT_TYPE.CHAIN
                || getType() == ObjectService.OBJECT_TYPE.BIN) {
            if (!isEquipable()) {
                throw new InvariantError("Weapons must be equipable");
            }
        }
    }

    @Override
    public void init(OBJECT_TYPE type, Vec3 pos) {

        checkInvariant();

        super.init(type, pos);

        checkInvariant();

        // post: getType(O) == type
        if (getType() != type) {
            throw new PostconditionError("Type not correctly set");
        }

        // post: getPos(O) == pos
        if (Vec3.distance(this.getPos(), pos) > Vec3.EPSILON) {
            throw new PostconditionError("Object pos not successfully set");
        }
    }

    @Override
    public void setPos(Vec3 pos) {

        checkInvariant();

        super.setPos(pos);

        checkInvariant();

        // post: getPos(O) == pos
        if (Vec3.distance(this.getPos(), pos) > Vec3.EPSILON) {
            throw new PostconditionError("Object pos not successfully set");
        }
    }

    @Override
    public void setInMap(boolean in) {

        checkInvariant();

        super.setInMap(in);

        checkInvariant();

        // post: isInMap(O) == in
        if (this.isInMap() != in) {
            throw new PostconditionError("isInMap not successfully set");
        }
    }
}
