package org.ghetto.contracts;

import org.ghetto.decorators.CharacterDecorator;
import org.ghetto.services.CharacterService;
import org.ghetto.services.ObjectService;
import org.ghetto.tools.Vec3;

public class CharacterContract extends CharacterDecorator {

    public CharacterContract(CharacterService delegate) {
        super(delegate);
    }

    public void checkInvariant() {

        // inv: getMoney(C) >= 0
        if (!(getMoney() >= 0)) {
            throw new InvariantError("Money is less than possible minimum");
        }
        // inv: getLife(C)) >= 0
        if (!(getLife() >= 0)) {
            throw new InvariantError("Life is less than possible minimum");
        }
        // inv: getDim(C)) >= 0
        if (!(getDim() >= 0)) {
            throw new InvariantError("Dim is less than possible minimum");
        }
    }

    @Override
    public void init(String name, Vec3 pos, float dim, float speed, int life,
            int power) {

        // pre: life >= 0
        if (!(life >= 0)) {
            throw new PreconditionError("Number of lives is negative or null");
        }

        // pre: power >= 0
        if (!(power >= 0)) {
            throw new PreconditionError("Power is negative or null");
        }

        // pre: name != null || name != ""
        if (name == null || "".equals(name)) {
            throw new PreconditionError("Name not set");
        }

        // pre: dim > 0
        if (!(dim > 0)) {
            throw new PreconditionError("Dim is =< 0");
        }

        // pre: speed > 0
        if (!(speed > 0)) {
            throw new PreconditionError("Speed is =< 0");
        }

        checkInvariant();

        super.init(name, pos, dim, speed, life, power);

        checkInvariant();

        // post: getLife(C) == life
        if (!(getLife() == life)) {
            throw new PostconditionError("Number of lives not correctly set");
        }

        // post: getPower(C) == power
        if (!(getPower() == power)) {
            throw new PostconditionError("Power not correctly set");
        }

        // post: getName(C) == name
        if (!(getName().equals(name))) {
            throw new PostconditionError("Name not correctly set");
        }

        // post: getPos(C) == pos
        if (Vec3.distance(getPos(), pos) > Vec3.EPSILON) {
            throw new PostconditionError("Position not correctly set");
        }

        // post: getDim(C) == dim
        if (!(getDim() == dim)) {
            throw new PostconditionError("Dim not correctly set");
        }

        // post: getSpeed(C) == speed
        if (!(getSpeed() == speed)) {
            throw new PostconditionError("Speed not correctly set");
        }

        // post: getHands(C) == null
        if (!(getHands() == null)) {
            throw new PostconditionError(
                    "Character initialized with a an object in hands");
        }
    }

    @Override
    public void setPos(Vec3 pos) {

        checkInvariant();

        super.setPos(pos);

        checkInvariant();

        // post: getPos(C) == pos
        if (Vec3.distance(getPos(), pos) > Vec3.EPSILON) {
            throw new PostconditionError("Position not correctly set");
        }
    }

    @Override
    public void addFreezeTime(int ms) {

        int freezeTime = getFreezeTime();

        checkInvariant();

        super.addFreezeTime(ms);

        checkInvariant();

        // post: getFreezeTime(C) == max(getFreezeTime(C)@pre + ms, 0)
        if (getFreezeTime() != Math.max(freezeTime + ms, 0)) {
            throw new PostconditionError("Freeze time not correctly set");
        }
    }

    @Override
    public void addLife(int life) {

        int lives = getLife();

        checkInvariant();

        super.addLife(life);

        checkInvariant();

        // post: getLife(C) == max(getLife(C)@pre + life, 0)
        if (getLife() != Math.max(lives + life, 0)) {
            throw new PostconditionError("Life not correctly set");
        }
    }

    @Override
    public void addMoney(int cash) {

        int money = getMoney();

        checkInvariant();

        super.addMoney(cash);

        checkInvariant();

        // post: getMoney(C) == max(getMoney(C)@pre + cash, 0)
        if (getMoney() != Math.max(money + cash, 0)) {
            throw new PostconditionError("Cash not correctly set");
        }
    }

    @Override
    public void stop() {

        checkInvariant();

        super.stop();

        checkInvariant();

        // post: isMoving(C) == false
        if (isMoving()) {
            throw new PostconditionError(
                    "Character has not been successfully stopped");
        }

        // post: isBeating(C) == false
        if (isBeating()) {
            throw new PostconditionError(
                    "Character has not been successfully stopped");
        }
    }

    @Override
    public boolean drift(Vec3 dir) {

        // pre: isDead(C) == false
        if (isDead()) {
            throw new PreconditionError("Character is dead");
        }

        Vec3 pos = getPos();

        checkInvariant();

        boolean drift = super.drift(dir);

        checkInvariant();

        // post: isMoving(C) == true
        if (!isMoving()) {
            throw new PostconditionError("isMoving not correctly set");
        }

        // post: getDriftDir(C) == dir
        if (Vec3.distance(getDriftDir(), dir) > Vec3.EPSILON) {
            throw new PostconditionError("Drift direction not correctly set");
        }

        // post: getPos(C) == dir + getPos(C)@pre
        if (Vec3.distance(getPos(), Vec3.add(dir, pos)) > Vec3.EPSILON) {
            throw new PostconditionError("Position not correctly set");
        }

        return drift;
    }

    @Override
    public boolean move(Vec3 dir) {

        // pre: isDead(C) == false
        if (isDead()) {
            throw new PreconditionError("Character is dead");
        }

        // pre: getFreezetime(C) == 0
        if (getFreezeTime() > 0) {
            throw new PreconditionError("Character is frozen");
        }

        Vec3 pos = getPos();

        checkInvariant();

        boolean move = super.move(dir);

        checkInvariant();

        // post: isMoving(C) == true
        if (!isMoving()) {
            throw new PostconditionError("isMoving not correctly set");
        }

        // post: getDir(C) == dir
        if (Vec3.distance(getDir(), dir) > Vec3.EPSILON) {
            throw new PostconditionError("Direction not correctly set");
        }

        // post: getPos(C) == dir + getPos(C)@pre
        if (Vec3.distance(getPos(), Vec3.add(dir, pos)) > Vec3.EPSILON) {
            throw new PostconditionError("Position not correctly set");
        }

        return move;
    }

    @Override
    public boolean beat() {

        // pre: isDead(C) == false
        if (isDead()) {
            throw new PreconditionError("Character is dead");
        }

        // pre: getFreezerTime(C) == 0
        if (getFreezeTime() > 0) {
            throw new PreconditionError("Character is frozen");
        }

        checkInvariant();

        boolean beat = super.beat();

        checkInvariant();

        // post: isMoving(C) == false
        if (isMoving()) {
            throw new PostconditionError("Charater has not stopped moving");
        }

        // post: isBeating(C) == true
        if (!isBeating()) {
            throw new PostconditionError("Beating not correctly set");
        }

        return beat;
    }

    @Override
    public boolean grab(ObjectService object) {

        // pre: getHands(C) == null
        if (getHands() != null) {
            throw new PreconditionError("Character have already a weapon");
        }

        // pre: isDead(C) == false
        if (isDead()) {
            throw new PreconditionError("Character is dead");
        }

        // pre: getFreezetime(C) == 0
        if (getFreezeTime() != 0) {
            throw new PreconditionError("Character is frozen");
        }

        checkInvariant();

        boolean grab = super.grab(object);

        checkInvariant();

        // post: getHands(C) == object
        if (getHands() != object) {
            throw new PostconditionError("Weapon has not been picked up");
        }

        return grab;
    }

    @Override
    public boolean drop() {

        // pre: getHands(C) != null
        if (getHands() == null) {
            throw new PostconditionError("Weapon not dropped");
        }

        // pre: isDead(C) == false
        if (isDead()) {
            throw new PreconditionError("Character is dead");
        }

        // pre: getFreezetime(C) == 0
        if (getFreezeTime() != 0) {
            throw new PreconditionError("Character is frozen");
        }

        checkInvariant();

        boolean drop = super.drop();

        checkInvariant();

        // post: getHands(C) == null
        if (getHands() != null) {
            throw new PostconditionError("Weapon not dropped");
        }

        return drop;
    }
}
