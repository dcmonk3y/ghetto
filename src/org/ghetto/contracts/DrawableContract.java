package org.ghetto.contracts;

import org.ghetto.decorators.DrawableDecorator;
import org.ghetto.services.DrawableService;

public class DrawableContract extends DrawableDecorator {

    public DrawableContract(DrawableService delegate) {
        super(delegate);
    }

}
