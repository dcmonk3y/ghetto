package org.ghetto.contracts;

import org.ghetto.decorators.BlockDecorator;
import org.ghetto.services.BlockService;

public class BlockContract extends BlockDecorator {

    public BlockContract(BlockService delegate) {
        super(delegate);
    }

    public void checkInvariant() {

    }

    @Override
    public void init(BLOCK_TYPE type) {

        checkInvariant();

        super.init(type);

        checkInvariant();

        // post: getType(B) == type
        if (getType() != type) {
            throw new PostconditionError("Type not correctly set");
        }
    }
}
