# GHETTO

********

## ant targets

* run game:

    $ ant run

* run game + embedded tests (contracts):

    $ ant embeddedTest

* run junit:

    $ ant test

* release game binaries:

    $ ant dist

## todo list

* write GameEngine specifications (in source code)
* write GameEngine test (junit)
* introduce bugs in badcomponents
* write all MBT (latex)
* write all formal specifications (latex)
* write report

## known bugs list

(empty)
