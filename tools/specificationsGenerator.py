#!/usr/bin/python2.7


import os
import sys
import argparse
import logging
import re


def process(path):
    try:
        with open(path) as f:
            code = f.read()
            logging.info('code extracted')
            code = format_code(code)
            logging.info('code formatted')
            specs = extract_specs(code)
            service = service_to_str(os.path.basename(path).split('.')[0])
            specs['service'] = service
            latex = to_latex(specs)
            logging.info('specifications extracted')
            with open(service + '.tex', 'w') as f:
                f.write(latex)
            logging.info(service + '.tex written')
    except Exception as e:
        logging.error(e)


def format_code(code):
    code = re.sub(r'\s', ' ', code)
    code = re.sub(r'  +', ' ', code)
    return code


def extract_specs(code):
    specs = {}
    allregex = {
            'observators' : r'observators(.*)end observators',
            'constructors' : r'constructors(.*)end constructors',
            'operators' : r'operators(.*)end operators'
            }
    for part in allregex:
        result = re.search(allregex[part], code)
        if result:
            specs[part] = extract_methods(result.group(1))
        else:
            specs[part] = []
            logging.warning(part + ' not found')
    return specs


def extract_methods(code):
    methods = []
    regex = r'public (\w+) (\w+) *\(([^\)]*)\) *;(.*)'
    while True:
        spec = re.search(regex, code)
        if spec:
            args = []
            for argcode in spec.group(3).split(','):
                 arg = re.search(r'(\w+)', argcode)
                 if arg:
                     args.append(arg.group(1).replace('_', '\_'))
            methods.append([
                spec.group(1).replace('_', '\_'), 
                spec.group(2).replace('_', '\_'), 
                args
                ])
            code = spec.group(len(spec.groups()))
        else:
            break;
    return methods


def to_latex(specs):
    latex = '\\section{' + specs['service'] + '}\n\n'
    latex += '\\begin{description}\n\n'
    used = get_used(specs)
    if used:
        latex += '\\item[use] : ' + list_to_str(used) + '\n\n'
    latex += '\\item[types] : ' + list_to_str(get_types(specs)) + '\n\n'
    for label in ['observators', 'constructors', 'operators']:
        if specs[label]:
            latex += '\\item[' + label + '] :\n\n'
            latex += get_methods(specs[label], specs['service'])
    latex += '\\item[observations] :\n\n'
    latex += get_observations(specs)
    latex += '\\end{description}\n'
    return latex


def service_to_str(name):
    return name.split('Service')[0]


def list_to_str(li):
    string = ''
    for i, elem in enumerate(li):
        string += str(elem)
        if i  + 1< len(li):
            string += ', '
    return string


def get_used(specs):
    used = []
    for label in ['observators', 'constructors', 'operators']:
        for part in specs[label]:
            used.append(part[0])
            used.extend(part[2])
    used = filter(lambda x: x.endswith('Service'), used)
    used = [service_to_str(x) for x in used]
    return list(set(used))


def get_types(specs):
    types = []
    for label in ['observators', 'constructors', 'operators']:
        for part in specs[label]:
            types.append(part[0])
            types.extend(part[2])
    types = filter(lambda x: x != 'void' and not x.endswith('Service'), types)
    return list(set(types))


def get_methods(part, service):
    methods = '\\begin{itemize}\n'
    for method in part:
        string = '\\item[] ' + method[1] + ' : ' + '[' + service + ']'
        for arg in method[2]:
            string += ' x ' + service_to_str(arg)
        if method[0] == 'void':
            string += '$\\to$ [' + service + ']'
        else:
            string += '$\\to$ ' + service_to_str(method[0])
        string += '\n'
        string += '%\\begin{itemize}\n'
        string += '%\\item[] \\textbf{pre} *** \\textbf{require} ***\n'
        string += '%\\end{itemize}\n'
        methods += string
    methods += '\\end{itemize}\n\n'
    return methods


def get_observations(specs):
    observations = '\\begin{itemize}\n'
    observations += '\\item[] [invariants]\n'
    observations += '%\\begin{itemize}\n'
    observations += '%\\item[] ***\n'
    observations += '%\\end{itemize}\n'
    for op in specs['operators']:
        observations += '\\item[] [' + op[1] + ']\n'
        observations += '%\\begin{itemize}\n'
        observations += '%\\item[] ***\n'
        observations += '%\\end{itemize}\n'
    observations +='\\end{itemize}\n\n'
    return observations


def get_args():
    parser = argparse.ArgumentParser(
            description = 'Extract specifications from java interfaces.'
            )
    parser.add_argument(
            'files',
            nargs = '+',
            help = 'files to be processed'
            )
    parser.add_argument(
            '-v',
            '--verbose',
            action = 'store_true',
            help = 'increase verbosity'
            )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    for path in map(os.path.abspath, args.files):
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        filename = os.path.basename(path) + ': '
        logging.basicConfig(
                level = level,
                format = '%(levelname)s: ' + filename + '%(message)s'
                )
        process(path)
